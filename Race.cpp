//Race.cpp
#include<iostream>
#include<cstdlib>
#include "Race.h"
#include "Horse.cpp"

using namespace std;

Race::Race(){
    Race::length = 100;
}
Race::Race(int length){
    Race::length = length;
}
void Race::printLane(int horseNum){
	int i;
    for(i = 0; i < Race::length; i++)
    {
        if(i == h[horseNum].getPosition())
            cout << horseNum;
        else
            cout << "-";
    }
    cout << endl;
}
void Race::start(){
    int i, winner;
    bool keepgoing = true;

    while(keepgoing)
    {
        for(i = 0; i < 5; i++)
        {
            if((rand() % 2) > 0)
            {
                h[i].advance();
            }
			//cout << string(100, '\n');
            printLane(i);

            if(h[i].getPosition()==length-1)
            {
                keepgoing = false;
                cout << "Horse #" << i << " is the winner!" << endl;
            }
        }
        cout << "Press enter to continue." << endl;
        cin.ignore();
    }
}
