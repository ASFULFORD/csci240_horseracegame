all: horsepower

horsepower: main.o 
	g++ main.o -o horsepower

main.o: main.cpp
	g++ -c main.cpp

clean:
	rm -rf *o horsepower
